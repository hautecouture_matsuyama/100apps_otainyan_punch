﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class Soundmanager : MonoBehaviour {

	public AudioSource[] Audios;
	//public UISprite SoundSprite;

	//public string SoundOnName = "btn-sOn";
	//public string SoundOffName = "btn-sOff";

	[SerializeField]
	private Sprite sprSndOn;
	[SerializeField]
	private Sprite sprSndOff;
	[SerializeField]
	private Image sprImg;

	[SerializeField]
	private float echoInterval;
	[SerializeField]
	private float echoDecrease;
	[SerializeField]
	private float echoMinimumVol;

	// Use this for initialization
	void Start () {
		if(PlayerPrefs.HasKey("sound")){
			if(PlayerPrefs.GetInt("sound") == 0)
			{
				//SoundSprite.spriteName = SoundOnName;
				sprImg.sprite = sprSndOn;
			}else{
				//SoundSprite.spriteName = SoundOffName;
				sprImg.sprite = sprSndOff;
			}
		}
		else PlayerPrefs.SetInt("sound",0);
	
		//PlayMusic();
	}

	public void PlayHitSound()
	{
		if(PlayerPrefs.GetInt("sound")	== 0)
			Audios[0].Play();
	}

	public void PlayOneShotHitSound()
	{
		if (PlayerPrefs.GetInt ("sound") == 0)
			Audios [0].PlayOneShot (Audios [0].clip, 1);
	}

	public void PlayGameoverSound()
	{
		/*
		if(PlayerPrefs.GetInt("sound")	== 0)
			Audios[1].Play();
		*/

		StartCoroutine (PlayGOSoundCrtn());
	}
	IEnumerator PlayGOSoundCrtn()
	{
		float volume = 1;

		while(volume > echoMinimumVol){
			if (PlayerPrefs.GetInt ("sound") == 0)
				Audios [1].PlayOneShot (Audios [1].clip, volume);

			volume *= echoDecrease;

			yield return new WaitForSeconds(echoInterval);
		}
	}

	public void PlayMusic()
	{
		if(PlayerPrefs.GetInt("sound")	== 0)
			Audios[2].Play();
	}

	public void PlayMoveSound(float waitTime)
	{
		/*if (PlayerPrefs.GetInt ("sound") == 0) {
			if(Audios [3].enabled == true){
				Audios [3].PlayOneShot (Audios [3].clip, 1);
			}
		}
		*/

		StartCoroutine (PlayMoveSoundCrtn(waitTime));
	}
	IEnumerator PlayMoveSoundCrtn(float waitTime){

		yield return new WaitForSeconds (waitTime);

		if (PlayerPrefs.GetInt ("sound") == 0) {
			if(Audios [3].enabled == true){
				Audios [3].Play();
			}
		}

	}

	public void PlayNormalSound()
	{
		if (PlayerPrefs.GetInt ("sound") == 0)
			Audios [4].Play();
	}

	public void StopMusic()
	{
		Audios[2].Stop();
	}

	public void StopMoveSound()
	{
		Audios[3].Stop();
	}

	void Sound()
	{
		if(PlayerPrefs.GetInt("sound")	== 0)
		{
			PlayerPrefs.SetInt("sound",1);

			//SoundSprite.spriteName = SoundOffName;
			sprImg.sprite = sprSndOff;
			StopMusic();
		}else
		{
			PlayerPrefs.SetInt("sound",0);
			//SoundSprite.spriteName = SoundOnName;
			sprImg.sprite = sprSndOn;
			PlayMusic();
		}
	}

	public void changeSndOnOff()
	{
		if(PlayerPrefs.GetInt("sound")	== 0)
		{
			PlayerPrefs.SetInt("sound",1);
			sprImg.sprite = sprSndOff;
			//StopMusic();
		}else
		{
			PlayerPrefs.SetInt("sound",0);
			sprImg.sprite = sprSndOn;
			//PlayMusic();
		}
	}
}
