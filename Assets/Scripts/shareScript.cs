﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class shareScript : MonoBehaviour {

	public Image shareButton;
	//public Image retryButton;
	
	public Sprite[] shareButtonSprites, shareButtonSprites_zhCN, shareButtonSprites_th;

	public Animator panelAnim;



	public void shareOpenStart(){
		panelAnim.Play ("HideAnim1");
	}
	public void shareOpenComplete()
	{
		Sprite changedSprite = shareButtonSprites[1];


		shareButton.sprite = changedSprite;
		//panelHide.SetActive (true);
	}

	public void shareCloseStart()
	{
		panelAnim.Play ("HideAnim2");
	}
	public void shareCloseComplete()
	{
		Sprite changedSprite = shareButtonSprites[0];
		
		
		shareButton.sprite = changedSprite;
		//panelHide.SetActive (false);
	}
}
